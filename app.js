import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import cors from 'cors';

import routes from './routes/index.js';

import models, { connectDb } from './models/index.js';

// Express config
let app = express();
dotenv.config();
// Headers
app.use(bodyParser.json());

// Middleware
app.use(cors());

// error handlers only dev env

// APIv2 routes
app.use('/api/message/', routes.webhook);
app.use('/api/message/', routes.message);

// Start
// const eraseDatabaseOnSync = true;

connectDb().then(async () => {
  // if (eraseDatabaseOnSync) {
  //   await Promise.all([models.Message.deleteMany({})]);
  // }

  app.listen(
    process.env.PORT || 3000,
    console.log('App running on port:', process.env.PORT || 3000),
  );
});

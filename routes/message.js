import express from 'express';
const routes = express.Router();

import models from '../models/index.js';

routes.get('/', (req, res) => {
  res.status(200).json({
    success: true,
    message: '.',
  });
});
// Get All Messages
routes.get('/all', (req, res) => {
  models.Message.find({}, (err, data) => {
    if (err) {
      console.log(err);
    }
    console.log(data);
    res.send(data);
  });
});

// Get message by SMS ID
routes.get('/sms/:id', (req, res) => {
  models.Message.findOne({ sms_id: req.params.id }, (err, data) => {
    if (err) {
      console.log(err);
    }
    console.log(data);
    res.send(data);
  });
});

// Find SMS by MongoDB object(_ID)
routes.get('/:id', (req, res) => {
  if (req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
    models.Message.findById(req.params.id, (err, data) => {
      if (!err) {
        console.log(data);
        res.status(200).json({
          success: true,
          data: data,
        });
      }
      console.log(err);
      throw err;
    });
  } else {
    res.status(500).json({
      success: false,
      message: `SMS with this ID: ${req.params.id.slice(
        0,
        15,
      )}.. were not found.`,
    });
  }
});

export default routes;

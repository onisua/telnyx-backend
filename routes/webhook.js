import express from 'express';
const routes = express.Router();

import models from '../models/index.js';

routes.post('/new', (req, res) => {
  let response = {};
  const data = req.body.data;
  if (typeof data === 'undefined') {
    res.status(500).json({
      success: false,
      message: 'Please provide a valid schema message.',
    });
    return;
  } else if (!data.payload.id) {
    res.status(500).json({
      success: false,
      message: 'Please enter message ID.',
    });
    return;
  } else if (!data.payload.from.phone_number) {
    res.status(500).json({
      success: false,
      message: 'Please enter sender phone number.',
    });
    return;
  } else if (!data.payload.to[0].phone_number) {
    res.status(500).json({
      success: false,
      message: 'Please enter correct recipient phone number.',
    });
    return;
  } else if (!data.payload.text) {
    res.status(500).json({
      success: false,
      message: 'Please enter your message.',
    });
    return;
  } else if (!data.payload.received_at) {
    res.status(500).json({
      success: false,
      message: 'Please enter a valid date timestamp.',
    });
    return;
  }

  response.sms_id = data.payload.id;
  response.from = data.payload.from.phone_number;
  response.to = data.payload.to[0].phone_number;
  response.message = data.payload.text;
  response.date = data.payload.received_at;

  const createNewMessages = async () => {
    const NewMessage = new models.Message({
      sms_id: response.sms_id,
      from: response.from,
      to: response.to,
      message: response.message,
      date: response.date,
    });

    await NewMessage.save((err, data) => {
      if (err) {
        console.log(err);
      }
      console.log(data);
      res.status('201').json({
        success: true,
        data: data,
      });
    });
  };
  createNewMessages();
});

export default routes;

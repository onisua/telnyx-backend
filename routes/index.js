import webhook from '../routes/webhook.js';
import message from '../routes/message.js';

export default {
  webhook,
  message,
};

import mongoose from 'mongoose';

import Message from '../models/message.js';

const connectDb = () => {
  return mongoose
    .connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log('Connected to Mongo!');
    })
    .catch((err) => {
      console.error('Error connecting to Mongo', err);
    });
};

const models = { Message };

export { connectDb };

export default models;

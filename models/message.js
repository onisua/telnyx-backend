import mongoose from 'mongoose';

let Schema = mongoose.Schema;

const messageSchema = new Schema({
  sms_id: { type: String },
  from: { type: String },
  to: { type: String },
  message: { type: String },
  date: { type: String },
});

const Message = mongoose.model('message', messageSchema);

export default Message;
